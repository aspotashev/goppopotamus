package goppopotamus

/*
#cgo LDFLAGS: -lgettextpo
#include <string.h>
#include <stdio.h>
#include <gettext-po.h>

// Linked list of gettext error messages
struct error_list {
	char *msg;
	struct error_list *next;
};

struct error_list *errors_root;

void add_error(char *msg) {
	struct error_list *new_root = malloc(sizeof(struct error_list));
	new_root->msg = msg;
	new_root->next = errors_root;

	errors_root = new_root;
}

void xerror_handler(
    int severity,
    po_message_t message, const char *filename, size_t lineno,
    size_t column, int multiline_p, const char *message_text) {
    size_t sz = strlen(message_text) + 100;
    if (filename) {
    	sz += strlen(filename);
    }

	char *msg = malloc(sz);
	*msg = '\0'; // start with empty string

	if (filename) {
		strcpy(msg, filename);

		if (lineno != (size_t)(-1)) {
			sprintf(msg + strlen(msg), ":%d", lineno);

			if (column != (size_t)(-1)) {
				sprintf(msg + strlen(msg), ":%d", column);
			}
		}
	}

	sprintf(msg + strlen(msg), ": {s%d} %s", severity, message_text);

	add_error(msg);

    if (severity == PO_SEVERITY_FATAL_ERROR) {
    	fprintf(stderr, "fatal error: %s\n", msg);
    	exit(1);
    }
}

void xerror2_handler(
    int severity,
    po_message_t message1, const char *filename1, size_t lineno1,
    size_t column1, int multiline_p1, const char *message_text1,
    po_message_t message2, const char *filename2, size_t lineno2,
    size_t column2, int multiline_p2, const char *message_text2) {
    xerror_handler(severity, message1, filename1, lineno1, column1, multiline_p1, message_text1);
}

po_file_t simplified_file_read(const char *path) {
	errors_root = NULL;

    struct po_xerror_handler xerror_handlers;
    xerror_handlers.xerror = xerror_handler;
    xerror_handlers.xerror2 = xerror2_handler;

    po_file_t file = po_file_read(path, &xerror_handlers);
    if (file == NULL) {
        add_error(strdup("failed to read .po/.pot file"));
        return NULL;
    }

    // Ensure that there is only one domain "messages"
    const char * const *domains = po_file_domains(file);
    if (strcmp(domains[0], "messages") != 0) {
        add_error(strdup("domain is not named \"messages\""));
    	po_file_free(file);
    	return NULL;
    }
    if (domains[1] != NULL) {
        add_error(strdup("more than one domain"));
    	po_file_free(file);
    	return NULL;
    }

    return file;
}

po_file_t simplified_file_write(po_file_t srcFile, const char *path) {
	errors_root = NULL;

    struct po_xerror_handler xerror_handlers;
    xerror_handlers.xerror = xerror_handler;
    xerror_handlers.xerror2 = xerror2_handler;

    po_file_t file = po_file_write(srcFile, path, &xerror_handlers);
    if (file == NULL) {
        add_error(strdup("failed to read .po/.pot file"));
        return NULL;
    }

    return file;
}

po_message_iterator_t po_message_iterator_default(po_file_t file) {
	return po_message_iterator(file, "messages");
}
*/
import "C"
import (
	"fmt"
	"log"
	"strings"
	"sync"
	"unsafe"
)

type Message struct {
	isObsolete  bool
	isFuzzy     bool
	msgid       string
	isPlural    bool
	msgidPlural string // valid if isPlural is true
	msgctxt     string
	msgstr      []string
	modified    bool
}

type BasicPOFile struct {
	p C.po_file_t

	headers         map[string]string
	headersLoaded   bool
	headersModified bool

	messages       []*Message
	messagesLoaded bool
}

func (m *Message) IsObsolete() bool {
	return m.isObsolete
}

func (m *Message) IsTranslated() bool {
	if m.isFuzzy {
		return false
	}

	// Consider message translated if at least one msgstr is translated:
	// that's how gettext tools do, but then they report an error for
	// missing argument in non-translated msgstrs.
	for _, val := range m.msgstr {
		if val != "" {
			return true
		}
	}

	return false
}

func (m *Message) Msgid() string {
	return m.msgid
}

func (m *Message) IsPlural() bool {
	return m.isPlural
}

func (m *Message) MsgidPlural() string {
	return m.msgidPlural
}

func (m *Message) Msgctxt() string {
	return m.msgctxt
}

func (m *Message) Msgstr() []string {
	return m.msgstr
}

func (m *Message) SetMsgstr(msgstr []string) {
	m.msgstr = msgstr
	m.modified = true
}

func importMessage(message C.po_message_t) *Message {
	m := Message{}
	m.isObsolete = C.po_message_is_obsolete(message) != 0
	m.isFuzzy = C.po_message_is_fuzzy(message) != 0

	m.msgid = C.GoString(C.po_message_msgid(message))

	cMsgidPlural := C.po_message_msgid_plural(message)
	if cMsgidPlural != nil {
		m.isPlural = true
		m.msgidPlural = C.GoString(cMsgidPlural)
	}

	m.msgctxt = C.GoString(C.po_message_msgctxt(message))

	if m.isPlural {
		for i := C.int(0); ; i++ {
			item := C.po_message_msgstr_plural(message, i)
			if item == nil {
				break
			}

			m.msgstr = append(m.msgstr, C.GoString(item))
		}
	} else {
		m.msgstr = append(m.msgstr, C.GoString(C.po_message_msgstr(message)))
	}

	return &m
}

func (m *Message) rewriteMessage(message C.po_message_t) {
	if m.modified {
		if m.isPlural {
			// Clear existing msgstr[:]
			for i := C.int(0); ; i++ {
				item := C.po_message_msgstr_plural(message, i)
				if item == nil {
					break
				}

				C.po_message_set_msgstr_plural(message, i, nil)
			}

			for i, str := range m.msgstr {
				cstr := C.CString(str)
				C.po_message_set_msgstr_plural(message, C.int(i), cstr)
				C.free(unsafe.Pointer(cstr))
			}
		} else {
			if len(m.msgstr) != 1 {
				panic(1)
			}

			cstr := C.CString(m.msgstr[0])
			C.po_message_set_msgstr(message, cstr)
			C.free(unsafe.Pointer(cstr))
		}

		m.modified = false
	}
}

func (f *BasicPOFile) AllMessages() []*Message {
	// TODO: use atomic compare and swap
	if f.messagesLoaded {
		return f.messages
	}

	iterator := C.po_message_iterator_default(f.p)
	defer C.po_message_iterator_free(iterator)

	// Skip header
	C.po_next_message(iterator)

	var msgs []*Message
	for {
		msg := C.po_next_message(iterator)
		if msg == nil {
			break
		}

		msgs = append(msgs, importMessage(msg))
	}

	f.messages = msgs
	f.messagesLoaded = true

	return msgs
}

func (f *BasicPOFile) rewriteAllMessages() {
	localMsgs := f.AllMessages()

	iterator := C.po_message_iterator_default(f.p)
	defer C.po_message_iterator_free(iterator)

	// Skip header
	C.po_next_message(iterator)

	i := 0
	for {
		msg := C.po_next_message(iterator)
		if msg == nil {
			break
		}

		localMsgs[i].rewriteMessage(msg)
		i++
	}

	if i != len(localMsgs) {
		panic("extra msgs")
	}
}

func (f *BasicPOFile) generateHeaderText() string {
	// List headers in the following order
	keys := []string{
		"Project-Id-Version",
		"Report-Msgid-Bugs-To",
		"POT-Creation-Date",
		"PO-Revision-Date",
		"Last-Translator",
		"Language-Team",
		"Language",
		"MIME-Version",
		"Content-Type",
		"Content-Transfer-Encoding",
		"X-Generator",
		"X-Qt-Contexts",
		"Plural-Forms",
		"Generated-By",
		"X-Environment",
		"X-Accelerator-Marker",
		"X-Text-Markup",
	}

	// Make a copy of headers map
	headers := make(map[string]string, len(f.headers))
	for key, value := range f.headers {
		headers[key] = value
	}

	var res string
	for _, key := range keys {
		if value, exists := headers[key]; exists {
			res += fmt.Sprintf("%s: %s\n", key, value)
			delete(headers, key)
		}
	}

	if len(headers) > 0 {
		log.Fatalf("unknown headers: %v", headers)
	}

	return res
}

func (f *BasicPOFile) rewriteHeaders() {
	if !f.headersModified {
		return
	}

	iterator := C.po_message_iterator_default(f.p)
	defer C.po_message_iterator_free(iterator)

	// Get header
	msg := C.po_next_message(iterator)
	if msg == nil {
		panic(1)
	}

	// Unfuzzy header
	C.po_message_set_fuzzy(msg, 0)

	cstr := C.CString(f.generateHeaderText())
	C.po_message_set_msgstr(msg, cstr)
	C.free(unsafe.Pointer(cstr))
}

func (f *BasicPOFile) ensureHeadersLoaded() {
	if f.headersLoaded {
		return
	}

	headerText := C.GoString(C.po_file_domain_header(f.p, nil))

	headers := make(map[string]string)
	for _, row := range strings.Split(headerText, "\n") {
		if row == "" {
			continue
		}

		colonPos := strings.Index(row, ": ")
		if colonPos < 0 {
			log.Fatalf("colon not found in row: %s", row)
		}

		key := row[:colonPos]
		if _, exists := headers[key]; exists {
			panic("duplicate key")
		}

		headers[key] = row[colonPos+2:]
	}

	f.headers = headers
	f.headersLoaded = true
}

func (f *BasicPOFile) SetHeader(key string, value string) {
	f.ensureHeadersLoaded()

	f.headers[key] = value
	f.headersModified = true
}

func (f *BasicPOFile) Header(key string) string {
	f.ensureHeadersLoaded()

	return f.headers[key]
}

func (f *BasicPOFile) Close() error {
	C.po_file_free(f.p)
	f.p = nil
	return nil
}

var gettextMutex sync.Mutex

func retrieveErrors() []string {
	var errs []string
	for {
		if C.errors_root == nil {
			break
		}

		errs = append(errs, C.GoString(C.errors_root.msg))
		C.free(unsafe.Pointer(C.errors_root.msg))

		new_root := C.errors_root.next
		C.free(unsafe.Pointer(C.errors_root))

		C.errors_root = new_root
	}

	return errs
}

func Load(path string) (*BasicPOFile, error) {
	// po_file_read is thread-unsafe
	gettextMutex.Lock()
	defer gettextMutex.Unlock()

	cPath := C.CString(path)
	defer C.free(unsafe.Pointer(cPath))

	file := C.simplified_file_read(cPath)

	errs := retrieveErrors()
	if file == nil {
		errs = append(errs, fmt.Sprintf("failed to load file %s", path))
	}

	if len(errs) > 0 {
		if file != nil {
			C.po_file_free(file)
		}

		return &BasicPOFile{}, fmt.Errorf("%s", strings.Join(errs, "\n"))
	} else {
		return &BasicPOFile{p: file}, nil
	}
}

func (f *BasicPOFile) Save(path string) error {
	f.rewriteAllMessages()
	f.rewriteHeaders()

	// po_file_write is thread-unsafe
	gettextMutex.Lock()
	defer gettextMutex.Unlock()

	cPath := C.CString(path)
	defer C.free(unsafe.Pointer(cPath))

	file := C.simplified_file_write(f.p, cPath)

	errs := retrieveErrors()
	if file == nil {
		errs = append(errs, fmt.Sprintf("failed to save file %s", path))
	}

	if len(errs) > 0 {
		return fmt.Errorf("%s", strings.Join(errs, "\n"))
	} else {
		return nil
	}
}
