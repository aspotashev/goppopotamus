package goppopotamus

import (
	"bytes"
	"github.com/stretchr/testify/assert"
	"io/ioutil"
	"testing"
)

func TestReadBasic(t *testing.T) {
	cat, err := Load("testdata/readbasic/media_www.po")
	assert.NoError(t, err)
	defer cat.Close()

	all := cat.AllMessages()

	assert.Equal(t, 116, len(all))

	assert.False(t, all[0].isFuzzy)
	assert.False(t, all[0].IsObsolete())
	assert.False(t, all[0].IsPlural())
	assert.Equal(t, []string{"Поддержка сайта:"}, all[0].Msgstr())

	assert.Equal(t,
		"Maintained by <a href=\"&#109;&#97;&#105;&#108;&#116;&#111;&#58;&#x77;e&#098;"+
			"ma&#115;t&#x65;&#x72;&#64;kde&#46;or&#x67;\">The KDE Webmaster</a><br />\n",
		all[1].Msgid())
}

func TestFillTemplate(t *testing.T) {
	cat, err := Load("testdata/readbasic/media_www.po")
	assert.NoError(t, err)
	defer cat.Close()

	cat.AllMessages()[0].SetMsgstr([]string{"321\n123"})

	outputPath := "/tmp/media_www.po.write"
	assert.NoError(t, cat.Save(outputPath))

	wrote, err := ioutil.ReadFile(outputPath)
	if err != nil {
		panic(err)
	}
	expected, err := ioutil.ReadFile("testdata/readbasic/media_www_set_msgstr.po")
	if err != nil {
		panic(err)
	}
	if !bytes.Equal(expected, wrote) {
		t.Errorf("files do not match")
	}
}

func TestEditHeaders(t *testing.T) {
	cat, err := Load("testdata/readbasic/media_www.po")
	assert.NoError(t, err)
	defer cat.Close()

	cat.SetHeader("Language", "x-test")

	outputPath := "/tmp/media_www.po.write"
	assert.NoError(t, cat.Save(outputPath))

	wrote, err := ioutil.ReadFile(outputPath)
	if err != nil {
		panic(err)
	}
	expected, err := ioutil.ReadFile("testdata/editheader/media_www.po")
	if err != nil {
		panic(err)
	}
	if !bytes.Equal(expected, wrote) {
		t.Errorf("files do not match")
	}
}
